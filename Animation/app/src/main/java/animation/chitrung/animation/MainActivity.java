package animation.chitrung.animation;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
 ImageView imageView;
 Button btnUp;
 Button btnDown;
 TranslateAnimation translateYAnimation;
 TranslateAnimation translateYAnimation2;
 float end = -100f;
 float start = 0f;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imageView = findViewById(R.id.img);
        btnUp = findViewById(R.id.btnUp);
        btnUp.setOnClickListener(this);
        btnDown = findViewById(R.id.btnDown);
        btnDown.setOnClickListener(this);
//        translateYAnimation = new TranslateAnimation(0f, 0f, 0f,-500f);
//        translateYAnimation.setRepeatCount(Animation.INFINITE);
//        translateYAnimation.setRepeatMode(Animation.REVERSE


       }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case (R.id.btnUp): upImg();break;
            case (R.id.btnDown): downImg();break;
        }
    }

    private void downImg() {
        translateYAnimation = new TranslateAnimation(0f, 0f, start,0f);
        translateYAnimation.setDuration(1000l);
        translateYAnimation.setFillEnabled(true);
        translateYAnimation.setFillAfter(true);
        imageView.startAnimation(translateYAnimation);
    }

    private void upImg() {
        translateYAnimation = new TranslateAnimation(0f, 0f, start,end);
        translateYAnimation.setDuration(1000l);
        translateYAnimation.setFillEnabled(true);
        translateYAnimation.setFillAfter(true);
        imageView.startAnimation(translateYAnimation);
        if (end <= -1000f){ start = 0f; end = 0f;}
        else{
        start = end;
        end += -100f;}
        Toast.makeText(this, "Y" + start, Toast.LENGTH_SHORT).show();


    }
}
