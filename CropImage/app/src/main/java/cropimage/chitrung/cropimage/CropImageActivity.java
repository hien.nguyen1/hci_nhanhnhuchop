package cropimage.chitrung.cropimage;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

public class CropImageActivity extends AppCompatActivity {

    private Uri imageUri;
    private CropImageView cropImageView;
    private Uri uri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crop_image);
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(this);

// start cropping activity for pre-acquired image saved on the device

        CropImage.activity(imageUri)
                .start(this);

// for fragment (DO NOT use `getActivity()`)
//        CropImage.activity()
//                .start(getContext(), this);
        cropImageView.setImageUriAsync(uri);
// or (prefer using uri for performance and better user experience)
//        cropImageView.setImageBitmap(bitmap);
        cropImageView.getCroppedImageAsync();
// or
//        Bitmap cropped = cropImageView.getCroppedImage();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }
}
