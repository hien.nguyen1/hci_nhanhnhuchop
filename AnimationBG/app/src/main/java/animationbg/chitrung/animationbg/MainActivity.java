package animationbg.chitrung.animationbg;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
TextView txtView;
Button btnChange;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtView = findViewById(R.id.txtView);
        btnChange = findViewById(R.id.btnChange);
        btnChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                ColorDrawable[] color = {new ColorDrawable(Color.RED), new ColorDrawable(Color.GREEN)};
//                TransitionDrawable trans = new TransitionDrawable(color);
//                txtView.setBackground(trans);
//                trans.startTransition(3000);
                final ValueAnimator valueAnimator1 = new ObjectAnimator().ofInt(view,"backgroundColor",Color.WHITE,Color.GREEN);
                valueAnimator1.setDuration(1500);
                valueAnimator1.setEvaluator(new ArgbEvaluator());
//                valueAnimator1.setIntValues(Color.WHITE, Color.GREEN);

                valueAnimator1.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator valueAnimator) {
                        txtView.setBackgroundColor((int)valueAnimator1.getAnimatedValue());
                    }
                });
                valueAnimator1.start();
            }
        });
    }
}
