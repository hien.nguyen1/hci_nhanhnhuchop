package hiennt.nhanhnhuchop.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import hiennt.nhanhnhuchop.R;
import hiennt.nhanhnhuchop.utils.Constants;
import hiennt.nhanhnhuchop.utils.SharePreferenceUtils;

public class DashboardAdultActivity extends AppCompatActivity implements View.OnClickListener{
    private VideoView videoView;
    private MediaController mediaController;
    private Button mBtnStart;
    private Button mBtnSetting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_adult);
        initialView();
        playVideo();
    }

    private void initialView(){
        videoView= findViewById(R.id.videoView_child);
        mBtnStart= findViewById(R.id.button_start_game_adult);
        mBtnSetting= findViewById(R.id.button_setting_game_adult);
        mBtnStart.setOnClickListener(this);
        mBtnSetting.setOnClickListener(this);
    }

    private void playVideo(){
        videoView.setVideoPath("android.resource://" + getPackageName() + "/" + R.raw.intro);
        mediaController = new MediaController(DashboardAdultActivity.this);
        mediaController.setAnchorView(videoView);
        videoView.setMediaController(mediaController);
        videoView.setMediaController(null);
        videoView.start();
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                videoView.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.button_start_game_adult:
                clickToVersionAdult();
                break;
            case R.id.button_setting_game_adult:
                showChangeModeDialog();
                break;
        }
    }

    private void clickToVersionAdult(){
        Intent intent = new Intent(DashboardAdultActivity.this, QuestionAdultActivity.class);
        startActivity(intent);
        //SharePreferenceUtils.saveIntSharedPreference(this, Constants.CONTANT_MODE, Constants.MODE.ALDULT);
    }

    private void showChangeModeDialog() {
        final Dialog dialog = new Dialog(DashboardAdultActivity.this);
        dialog.setContentView(R.layout.alert_dialog_change_mode);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Button btnOK = dialog.findViewById(R.id.button_oke);
        Button btnContinute = dialog.findViewById(R.id.button_no);
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intentToMain();
                dialog.dismiss();
            }
        });
        btnContinute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //continuteGame();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void intentToMain(){
        Intent intent = new Intent(DashboardAdultActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
