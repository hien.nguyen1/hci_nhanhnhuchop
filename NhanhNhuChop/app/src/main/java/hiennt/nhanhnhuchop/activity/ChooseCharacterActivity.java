package hiennt.nhanhnhuchop.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import hiennt.nhanhnhuchop.R;

public class ChooseCharacterActivity extends BaseActivity implements View.OnClickListener{
    private ImageView mImg1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_character);
        initialView();
    }

    private void initialView(){
        mImg1= findViewById(R.id.image_view_choose_1);
        mImg1.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.image_view_choose_1:
                clickToChoose1();
                break;
        }
    }

    private void clickToChoose1(){
        Intent intent = new Intent(ChooseCharacterActivity.this, QuestionActivity.class);
        startActivity(intent);
        finish();
    }
}
