package hiennt.nhanhnhuchop.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.VideoView;

import hiennt.nhanhnhuchop.R;

public class DashboardChildActivity extends AppCompatActivity implements View.OnClickListener{
    private VideoView videoView;
    private MediaController mediaController;
    private Button mBtnStart;
    private Button mBtnSetting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_child);
        initialView();
        playVideo();
    }

    private void initialView(){
        videoView= findViewById(R.id.videoView_child);
        mBtnStart= findViewById(R.id.button_start_game_childrent);
        mBtnSetting= findViewById(R.id.button_setting_game_childent);
        mBtnStart.setOnClickListener(this);
        mBtnSetting.setOnClickListener(this);
    }

    private void playVideo(){
        videoView.setVideoPath("android.resource://" + getPackageName() + "/" + R.raw.intro2);
        mediaController = new MediaController(DashboardChildActivity.this);
        mediaController.setAnchorView(videoView);
        videoView.setMediaController(mediaController);
        videoView.setMediaController(null);
        videoView.start();
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                videoView.setVisibility(View.GONE);
            }
        });
    }

    private void showChangeModeDialog() {
        final Dialog dialog = new Dialog(DashboardChildActivity.this);
        dialog.setContentView(R.layout.alert_dialog_change_mode);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Button btnOK = dialog.findViewById(R.id.button_oke);
        Button btnContinute = dialog.findViewById(R.id.button_no);
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intentToMain();
                dialog.dismiss();
            }
        });
        btnContinute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //continuteGame();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.button_start_game_childrent:
                intentToQuestionForChildren();
                break;
            case R.id.button_setting_game_childent:
                showChangeModeDialog();
                break;
        }
    }

    private void intentToQuestionForChildren(){
        Intent intent = new Intent(DashboardChildActivity.this, QuestionActivity.class);
        startActivity(intent);
    }

    private void intentToMain(){
        Intent intent = new Intent(DashboardChildActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
