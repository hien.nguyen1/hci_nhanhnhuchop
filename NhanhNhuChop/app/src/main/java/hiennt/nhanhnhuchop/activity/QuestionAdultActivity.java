package hiennt.nhanhnhuchop.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.speech.RecognizerIntent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import hiennt.nhanhnhuchop.R;
import hiennt.nhanhnhuchop.presenter.GetAnswerPresenter;
import hiennt.nhanhnhuchop.presenter.GetQuestionPresenter;
import hiennt.nhanhnhuchop.room.entities.Answer;
import hiennt.nhanhnhuchop.room.entities.Question;
import hiennt.nhanhnhuchop.utils.Constants;
import hiennt.nhanhnhuchop.utils.TypeWriter;
import hiennt.nhanhnhuchop.view.GetAnswerView;
import hiennt.nhanhnhuchop.view.GetQuestionView;
import pl.droidsonroids.gif.GifImageView;

public class QuestionAdultActivity extends BaseActivity implements View.OnClickListener, GetAnswerView, GetQuestionView {
    private CountDownTimer mCountDownTimer;
    private TextView mTxtCountDown;
    private TypeWriter mTwContentQuestion;
    private List<Question> mListQuestion;
    private GetQuestionPresenter mGetQuestionPresenter;
    private List<Answer> mListAnswer;
    private GetAnswerPresenter mGetAnswerPresenter;
    private Button mBtnAnswer;
    private EditText mEdtAnswer;
    private ImageView mImgVoice;
    private int mQuestionId = 0;
    private int mPos = 0;
    private int mScore = 0;
    private int mFinalScore = 0;
    private int[] mArrayTxtPoint = {R.id.txt_1, R.id.txt_2, R.id.txt_3, R.id.txt_4, R.id.txt_5, R.id.txt_6, R.id.txt_7, R.id.txt_8, R.id.txt_9, R.id.txt_10};
    private MediaPlayer mMediaPlayer;
    private ImageView mImg_repeat;
    private ImageView mImgAction;
    private long mTotalTime = 120000;
    TranslateAnimation translateYAnimation;
    float end = -50f;
    float start = 0f;
    private ImageView mImgSlideCar;
    private LinearLayout mLnlSliderCar;
    Vibrator vibrate;
    Animation animationFlicker;
    private MediaPlayer mClockTicking;
    private GifImageView mGifEmoji;
    private TextView mTxtVoice;
    private TextView mTxtWrite;
    private LinearLayout mlnlWrite;
    private Switch mSwitchMode;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_adult);
        initialView();
        countDownTimer();
        initialData();
        mClockTicking = new MediaPlayer();

    }

    private void initialView() {
        mTxtCountDown = findViewById(R.id.text_count_down);
        mTwContentQuestion = findViewById(R.id.typewriter_content_question);
        mEdtAnswer = findViewById(R.id.edit_text_answer);
        mBtnAnswer = findViewById(R.id.button_answer);
        mImgVoice = findViewById(R.id.image_voice);
        mBtnAnswer.setOnClickListener(this);
        mImgVoice.setOnClickListener(this);
        mImg_repeat = findViewById(R.id.image_repeat);
        mImg_repeat.setOnClickListener(this);
        mImgAction = findViewById(R.id.imgage_view_action);
        mMediaPlayer = new MediaPlayer();
        mImgSlideCar = findViewById(R.id.image_slide_car);
        mLnlSliderCar = findViewById(R.id.lnl_slidecar);
        mLnlSliderCar.setVisibility(View.INVISIBLE);
        vibrate = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        mGifEmoji = findViewById(R.id.gif_emoji);
        mTxtVoice = findViewById(R.id.text_voice);
        mTxtWrite = findViewById(R.id.text_write);
        mlnlWrite = findViewById(R.id.lnl_write);
        mSwitchMode= findViewById(R.id.switch_mode);
        mSwitchMode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                changeMode();
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.image_voice:
                promptSpeechInput();
                break;
            case R.id.button_answer:
                checkAnswer();
                break;
            case R.id.image_repeat:
                playAudio(mPos);
                break;
        }
    }

    private void changeMode() {
        Animation fadeIn = new AlphaAnimation(0, 1);
        fadeIn.setInterpolator(new DecelerateInterpolator()); //add this
        fadeIn.setDuration(1000);
        if(!mSwitchMode.isChecked()){
            mSwitchMode.setThumbResource(R.mipmap.ic_voice_small);
            mlnlWrite.setVisibility(View.GONE);
            mImgVoice.setVisibility(View.VISIBLE);
            mImgVoice.startAnimation(fadeIn);
            mTxtWrite.setVisibility(View.GONE);
            mTxtVoice.setVisibility(View.VISIBLE);
            mTxtVoice.startAnimation(fadeIn);
        }else{
            mSwitchMode.setThumbResource(R.mipmap.ic_write_small);
            mImgVoice.setVisibility(View.GONE);
            mlnlWrite.setVisibility(View.VISIBLE);
            mlnlWrite.startAnimation(fadeIn);
            mTxtVoice.setVisibility(View.GONE);
            mTxtWrite.setVisibility(View.VISIBLE);
            mTxtWrite.startAnimation(fadeIn);
        }
    }

    private void countDownTimer() {
        mCountDownTimer = new CountDownTimer(mTotalTime, 1000) {
            @Override
            public void onTick(long l) {
                mTotalTime = l;
                long seconds = l / 1000;
                int minute = (int) (seconds / 60);
                int s = (int) (seconds % 60);
                if (minute == 0 && s < 10) {
                    animationFlicker = AnimationUtils.loadAnimation(QuestionAdultActivity.this, R.anim.flicker);
                    mTxtCountDown.startAnimation(animationFlicker);
                    mTxtCountDown.setTextColor(Color.RED);
                    mTxtCountDown.setText(minute + ": " + "0"+ s);
                    mClockTicking.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    if (!mClockTicking.isPlaying()) {
                        mClockTicking = MediaPlayer.create(getApplicationContext(), R.raw.clockticking);
                        mClockTicking.start();
                    }
                } else {
                    if(s < 10){
                        mTxtCountDown.setTextColor(Color.WHITE);
                        mTxtCountDown.setText(minute + ": 0" + s);
                    }else {
                        mTxtCountDown.setTextColor(Color.WHITE);
                        mTxtCountDown.setText(minute + ": " + s);
                    }
                }
            }

            @Override
            public void onFinish() {
                if (!((Activity) QuestionAdultActivity.this).isFinishing()) {
                    try {
                        mTxtCountDown.setTextSize(20);
                        mTxtCountDown.setText("Hết giờ");
                        mClockTicking.release();
                        showFinalScoreDialog();
                    } catch (WindowManager.BadTokenException e) {
                        Log.e("WindowManagerBad ", e.toString());
                    }
                }
            }
        }.start();
    }

    private void promptSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                getString(R.string.speech_prompt));
        try {
            startActivityForResult(intent, Constants.REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getApplicationContext(),
                    getString(R.string.speech_not_supported),
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case Constants.REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    mEdtAnswer.setText(result.get(0));
                    checkAnswer();
                }
                break;
            }

        }
    }

    private void initialData() {
        mGetQuestionPresenter = new GetQuestionPresenter(this, getApplication(), this);
        mGetQuestionPresenter.getQuestion(2);
        mGetAnswerPresenter = new GetAnswerPresenter(this, getApplication(), this);
    }

    @Override
    public void showAnswerSuccess(List<Answer> answerList) {
        mListAnswer = answerList;
    }

    @Override
    public void showQuestion(List<Question> question) {
        mListQuestion = question;
        Collections.shuffle(mListQuestion);
        mQuestionId = mListQuestion.get(mPos).getQuestionId();
        mGetAnswerPresenter.getAnswer(mQuestionId);
        setTextQuestion(mPos);
        playAudio(mPos);
    }

    @Override
    public void showError() {
        Toast.makeText(QuestionAdultActivity.this, "Error", Toast.LENGTH_LONG).show();
    }

    private void setTextQuestion(int position) {
        mImgAction.setImageResource(R.mipmap.img_truonggiang1);
        mGifEmoji.setImageDrawable(null);
        String contentQuestion = mListQuestion.get(position).getQuestionContent();
        mTwContentQuestion.setText("");
        mTwContentQuestion.setCharacterDelay(50);
        mTwContentQuestion.animateText(contentQuestion);
        mEdtAnswer.setText("");
    }

    private void checkAnswer() {
        boolean check = false;
        String answer = mEdtAnswer.getText().toString().trim().toUpperCase();
        if (answer != null && !answer.isEmpty()) {
            for (int i = 0; i < mListAnswer.size(); i++) {
                if (answer.equalsIgnoreCase(mListAnswer.get(i).getAnswerContent())) {
                    check = true;
                    break;
                }
            }
            if (check) {
                mEdtAnswer.setEnabled(false);
                mBtnAnswer.setEnabled(false);
                setVibrateForAnswer();
                mImgAction.setImageResource(R.mipmap.img_truonggiang3);
                mGifEmoji.setImageResource(R.mipmap.gif_like);
                mScore++;
                if (mScore > mFinalScore) {
                    mFinalScore = mScore;
                }
                playAudioCheck("voice_true");
                setProgressPoint(mScore);
                //setNextQuestion();
            } else {
                mEdtAnswer.setEnabled(false);
                mBtnAnswer.setEnabled(false);
                mImgAction.setImageResource(R.mipmap.img_truonggiang2);
                mGifEmoji.setImageResource(R.mipmap.gif_sad);
                if (mScore > 0) {
                    setVibrateForAnswer();
                }
                mScore = 0;
                playAudioCheck("voice_false");
                setProgressPoint(mScore);
                //setNextQuestion();
            }
        } else {
            Toast.makeText(QuestionAdultActivity.this, "Vui lòng trả lời câu hỏi!", Toast.LENGTH_LONG).show();
        }
    }

    private void setProgressPoint(int point) {
        TextView txtPoint = new TextView(this);
        if (point > 0 && point <= 10) {
            for (int i = 0; i < point; i++) {
                txtPoint = findViewById(mArrayTxtPoint[i]);
                txtPoint.setVisibility(View.VISIBLE);
                txtPoint.setBackgroundColor(getResources().getColor(R.color.light_green));
            }
            mLnlSliderCar.setVisibility(View.VISIBLE);
            translateYAnimation = new TranslateAnimation(0f, 0f, start, end);
            translateYAnimation.setDuration(1000l);
            translateYAnimation.setFillEnabled(true);
            translateYAnimation.setFillAfter(true);
            mImgSlideCar.startAnimation(translateYAnimation);
            start = end;
            end += -50f;
        } else if (point == 0) {
            for (int i = 0; i < mArrayTxtPoint.length; i++) {
                txtPoint = findViewById(mArrayTxtPoint[i]);
                txtPoint.setVisibility(View.INVISIBLE);
            }
            translateYAnimation = new TranslateAnimation(0f, 0f, start, 0f);
            translateYAnimation.setDuration(1000l);
            translateYAnimation.setFillEnabled(true);
            translateYAnimation.setFillAfter(true);
            mImgSlideCar.startAnimation(translateYAnimation);
            start = 0;
            end = -50f;

        } else {
            //win
            showFinalScoreDialog();
        }
    }

    private void showFinalScoreDialog() {
        final Dialog dialog = new Dialog(QuestionAdultActivity.this);
        dialog.setContentView(R.layout.alert_notify_end_game);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        TextView txtFinalScore = dialog.findViewById(R.id.text_view_final_score);
        txtFinalScore.setText(mFinalScore + " câu");
        ImageView imgOK = dialog.findViewById(R.id.image_close);
        ImageView imgContinute = dialog.findViewById(R.id.image_continute);
        imgOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                QuestionAdultActivity.this.finish();
            }
        });
        imgContinute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                continuteGame();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void setNextQuestion() {
        mPos++;
        if (mPos < mListQuestion.size()) {
            mQuestionId = mListQuestion.get(mPos).getQuestionId();
            setTextQuestion(mPos);
            playAudio(mPos);
            mGetAnswerPresenter.getAnswer(mQuestionId);
        } else {
            showFinalScoreDialog();
        }
    }

    @Override
    protected void onDestroy() {
        mCountDownTimer.cancel();
        releasePlayer();
        super.onDestroy();
    }

    private void playAudio(int position) {
        mImg_repeat.setEnabled(false);
        String link = mListQuestion.get(position).getVoice();
        int resID = getResources().getIdentifier(link, "raw", getPackageName());

        mMediaPlayer.reset();
        if (!mMediaPlayer.isPlaying()) {
            mMediaPlayer = MediaPlayer.create(getApplicationContext(), resID);
            mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

            mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    mp.release();
                    mMediaPlayer = new MediaPlayer();
                    mImg_repeat.setEnabled(true);
                }
            });

            mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.start();
                }
            });// the song is a filename which i have pasted inside a folder **raw** created under the **res** folder.//
            //mMediaPlayer.start();
            mMediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
                    Log.e("MediaPlayer", "Error");
                    return false;
                }
            });
        } else {
            releasePlayer();
            mMediaPlayer = MediaPlayer.create(getApplicationContext(), resID);// the song is a filename which i have pasted inside a folder **raw** created under the **res** folder.//
            mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

            mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    mp.release();
                }
            });

            mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.start();
                }
            });
            mMediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
                    Log.e("MediaPlayer", "Error");
                    return false;
                }
            });
        }
    }

    private void releasePlayer() {
        if (mMediaPlayer != null) {
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
    }

    private void playAudioCheck(String link) {
        stopCountDown();
        int resID = getResources().getIdentifier(link, "raw", getPackageName());

        mMediaPlayer.reset();
        if (!mMediaPlayer.isPlaying()) {
            mMediaPlayer = MediaPlayer.create(getApplicationContext(), resID);// the song is a filename which i have pasted inside a folder **raw** created under the **res** folder.//
            mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

            mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    mp.release();
                    mMediaPlayer = new MediaPlayer();
                    countDownTimer();
                    setNextQuestion();
                    mEdtAnswer.setEnabled(true);
                    mBtnAnswer.setEnabled(true);
                }
            });

            mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.start();
                }
            });
        } else {
            releasePlayer();
            mMediaPlayer = MediaPlayer.create(getApplicationContext(), resID);// the song is a filename which i have pasted inside a folder **raw** created under the **res** folder.//
            mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

            mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    mp.release();
                    countDownTimer();
                    setNextQuestion();
                }
            });

            mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.start();
                }
            });
        }
    }

    private void stopCountDown() {
        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
        }
    }

    private void continuteGame() {
        mTxtCountDown.clearAnimation();
        mLnlSliderCar.setVisibility(View.INVISIBLE);
        start = 0;
        end = -50f;
        mTotalTime = 120000;
        mFinalScore = 0;
        mScore = 0;
        setProgressPoint(mScore);
        mPos = 0;
        mGetQuestionPresenter.getQuestion(2);
        mCountDownTimer.cancel();
        countDownTimer();
        mClockTicking = new MediaPlayer();
    }

    private void setVibrateForAnswer() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            vibrate.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            vibrate.vibrate(500);
        }
    }
}
