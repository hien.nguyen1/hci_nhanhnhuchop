package hiennt.nhanhnhuchop.activity;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.CountDownTimer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Collections;
import java.util.List;

import hiennt.nhanhnhuchop.R;
import hiennt.nhanhnhuchop.presenter.GetAnswerPresenter;
import hiennt.nhanhnhuchop.presenter.GetQuestionPresenter;
import hiennt.nhanhnhuchop.room.entities.Answer;
import hiennt.nhanhnhuchop.room.entities.Question;
import hiennt.nhanhnhuchop.view.GetAnswerView;
import hiennt.nhanhnhuchop.view.GetQuestionView;

public class QuestionActivity extends BaseActivity implements View.OnClickListener, GetQuestionView, GetAnswerView{
    private ImageView mImgAnwser1;
    private CountDownTimer mCountDownTimer;
    private TextView mTxtCountDown;
    private TextView mTxtContentQuestion;
    private List<Question> mListQuestion;
    private GetQuestionPresenter mGetQuestionPresenter;
    private ImageView mImgAnwser2;
    private ImageView mImgAnwser3;
    private ImageView mImgAnwser4;
    private List<Answer> mListAnswer;
    private GetAnswerPresenter mGetAnswerPresenter;
    private int mAnswerTrue;
    private int mScore=0;
    private int mFinalScore=0;
    private int mQuestionId=1;
    private int[] mArrayTxtPoint={R.id.txt_point1,R.id.txt_point2,R.id.txt_point3,R.id.txt_point4,R.id.txt_point5,R.id.txt_point6,R.id.txt_point7,R.id.txt_point8,R.id.txt_point9,R.id.txt_point10};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);
        initialView();
//        initialData();
//        countDownTimer();
    }

    private void initialView(){
        mImgAnwser1= findViewById(R.id.img_view_anwser1);
        mImgAnwser1.setOnClickListener(this);
        mTxtCountDown= findViewById(R.id.text_count_down);
        mTxtContentQuestion= findViewById(R.id.text_content_question);
        mImgAnwser2= findViewById(R.id.img_view_anwser2);
        mImgAnwser3= findViewById(R.id.img_view_anwser3);
        mImgAnwser4= findViewById(R.id.img_view_anwser4);
        mImgAnwser2.setOnClickListener(this);
        mImgAnwser3.setOnClickListener(this);
        mImgAnwser4.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.img_view_anwser1:
                clickCheckAnswer(1);
                break;
            case R.id.img_view_anwser2:
                clickCheckAnswer(2);
                break;
            case R.id.img_view_anwser3:
                clickCheckAnswer(3);
                break;
            case R.id.img_view_anwser4:
                clickCheckAnswer(4);
                break;
        }
    }

    private void countDownTimer() {
        mCountDownTimer = new CountDownTimer(120000, 1000) {
            @Override
            public void onTick(long l) {
                long seconds= l/1000;
                int minute= (int) (seconds/60);
                int s= (int) (seconds%60);
                mTxtCountDown.setText(minute+": "+s);
            }

            @Override
            public void onFinish() {
                if (!((Activity) QuestionActivity.this).isFinishing()) {
                    try {
                        showFinalScoreDialog();
                    } catch (WindowManager.BadTokenException e) {
                        Log.e("WindowManagerBad ", e.toString());
                    }
                }
            }
        }.start();
    }

    @Override
    public void showQuestion(List<Question> question) {
        mListQuestion= question;
        Collections.shuffle(mListQuestion);
        setTextQuestion(mQuestionId);
    }

    private void initialData(){
        mGetQuestionPresenter= new GetQuestionPresenter(this, getApplication(), this);
        mGetQuestionPresenter.getQuestion(1);
        mGetAnswerPresenter= new GetAnswerPresenter(this, getApplication(), this);
        mGetAnswerPresenter.getAnswer(mQuestionId);
    }

    @Override
    public void showError() {
        Toast.makeText(QuestionActivity.this,"Error",Toast.LENGTH_LONG).show();
    }

    private void setImgForAnswer(){
        String link1=mListAnswer.get(0).getAnswerContent();
        mImgAnwser1.setImageResource(getResources().getIdentifier(link1,"mipmap",getPackageName()));
        String link2=mListAnswer.get(1).getAnswerContent();
        mImgAnwser2.setImageResource(getResources().getIdentifier(link2,"mipmap",getPackageName()));
        String link3=mListAnswer.get(2).getAnswerContent();
        mImgAnwser3.setImageResource(getResources().getIdentifier(link3,"mipmap",getPackageName()));
        String link4=mListAnswer.get(3).getAnswerContent();
        mImgAnwser4.setImageResource(getResources().getIdentifier(link4,"mipmap",getPackageName()));
    }

    @Override
    public void showAnswerSuccess(List<Answer> answerList) {
        mListAnswer=answerList;
        Collections.shuffle(mListAnswer);
        setImgForAnswer();
        getAnswerTrue();
    }

    private void getAnswerTrue(){
        for (int i=0; i<mListAnswer.size();i++){
            if(mListAnswer.get(i).isAnswerCheck()){
                mAnswerTrue=i+1;
                break;
            }
        }
    }

    private void clickCheckAnswer(int answer){
        if(answer== mAnswerTrue){
            mScore++;
            if(mScore>mFinalScore){
                mFinalScore= mScore;
            }
            setProgressPoint(mScore);
            mQuestionId++;
            if(mQuestionId>mListQuestion.size()){
                Toast.makeText(QuestionActivity.this,"Hết câu hỏi rồi cu !",Toast.LENGTH_LONG).show();
            }else {
                setTextQuestion(mQuestionId);
                mGetAnswerPresenter.getAnswer(mQuestionId);
            }
        }else {
            mScore=0;
            setProgressPoint(mScore);
            mQuestionId++;
            if(mQuestionId>mListQuestion.size()){
                Toast.makeText(QuestionActivity.this,"Hết câu hỏi rồi cu !",Toast.LENGTH_LONG).show();
            }else {
                setTextQuestion(mQuestionId);
                mGetAnswerPresenter.getAnswer(mQuestionId);
            }
            }
    }

    private void setTextQuestion(int questionId){
        String contentQuestion= mListQuestion.get(questionId-1).getQuestionContent();
        mTxtContentQuestion.setText(contentQuestion);
    }

    private void setProgressPoint(int point){
        TextView txtPoint = new TextView(this);
        if(point>0&&point<10) {
            for (int i = 0; i < point; i++) {
                txtPoint = findViewById(mArrayTxtPoint[i]);
                txtPoint.setBackgroundColor(getResources().getColor(R.color.colorCharal));
            }
        }else if(point==0){
            for (int i=0; i<mArrayTxtPoint.length;i++){
                txtPoint = findViewById(mArrayTxtPoint[i]);
                txtPoint.setBackgroundColor(getResources().getColor(R.color.white));
            }
        }
        else {
            //win
        }
    }

    private void showFinalScoreDialog() {
        final Dialog dialog = new Dialog(QuestionActivity.this);
        dialog.setContentView(R.layout.alert_notify_end_game);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView txtFinalScore= dialog.findViewById(R.id.text_view_final_score);
        txtFinalScore.setText(mFinalScore+" điểm");
        Button btnOK = dialog.findViewById(R.id.image_close);
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    protected void onDestroy() {
        //mCountDownTimer.cancel();
        super.onDestroy();
    }
}
