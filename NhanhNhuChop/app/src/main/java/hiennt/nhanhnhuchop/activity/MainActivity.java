package hiennt.nhanhnhuchop.activity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import hiennt.nhanhnhuchop.R;
import hiennt.nhanhnhuchop.presenter.AddAnswerPresenter;
import hiennt.nhanhnhuchop.presenter.AddQuestionPresenter;
import hiennt.nhanhnhuchop.room.entities.Answer;
import hiennt.nhanhnhuchop.room.entities.Question;
import hiennt.nhanhnhuchop.utils.Constants;
import hiennt.nhanhnhuchop.utils.SharePreferenceUtils;
import hiennt.nhanhnhuchop.view.AddAnswerView;
import hiennt.nhanhnhuchop.view.AddQuestionView;

public class MainActivity extends BaseActivity implements View.OnClickListener, AddQuestionView, AddAnswerView {

    private MediaPlayer mMediaPlayer;
    private Button mBtnChildren;
    private Button mBtnAdult;
    private ImageView mImgChildren;
    private ImageView mImgAdult;
    private AddQuestionPresenter mAddQuestionPresenter;
    private AddAnswerPresenter mAddAnswerPresenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialView();
        checkAddedQuestion();
    }

    private void initialView(){
        mBtnChildren= findViewById(R.id.button_children);
        mBtnChildren.setOnClickListener(this);
        mBtnAdult= findViewById(R.id.button_adjust);
        mBtnAdult.setOnClickListener(this);
        mImgChildren= findViewById(R.id.image_view_sound_children);
        mImgAdult= findViewById(R.id.image_view_sound_adult);
        mImgAdult.setOnClickListener(this);
        mImgChildren.setOnClickListener(this);
        mMediaPlayer= new MediaPlayer();
    }

    @Override
    protected void onDestroy() {
        releasePlayer();
        super.onDestroy();
    }

    private void playAudioChildren(){
        if(!mMediaPlayer.isPlaying()) {
            mMediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.version_children);// the song is a filename which i have pasted inside a folder **raw** created under the **res** folder.//
            mMediaPlayer.start();
        }else {
            releasePlayer();
            mMediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.version_children);// the song is a filename which i have pasted inside a folder **raw** created under the **res** folder.//
            mMediaPlayer.start();
        }
    }

    private void playAudioAdult(){
        if(!mMediaPlayer.isPlaying()) {
            mMediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.version_adult);// the song is a filename which i have pasted inside a folder **raw** created under the **res** folder.//
            mMediaPlayer.start();
        }else {
            releasePlayer();
            mMediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.version_adult);// the song is a filename which i have pasted inside a folder **raw** created under the **res** folder.//
            mMediaPlayer.start();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.button_children:
                clickToVersionChildren();
                break;
            case R.id.button_adjust:
                clickToVersionAdult();
                break;
            case R.id.image_view_sound_children:
                playAudioChildren();
                break;
            case R.id.image_view_sound_adult:
                playAudioAdult();
                break;
        }
    }

    private void releasePlayer(){
        if(mMediaPlayer!= null){
            mMediaPlayer.release();
            mMediaPlayer= null;
        }
    }

    private void clickToVersionChildren(){
        Intent intent = new Intent(MainActivity.this, DashboardChildActivity.class);
        startActivity(intent);
        SharePreferenceUtils.saveIntSharedPreference(this,Constants.CONTANT_MODE, Constants.MODE.CHILDREN);
        finish();
    }

    private void clickToVersionAdult(){
        Intent intent = new Intent(MainActivity.this, DashboardAdultActivity.class);
        startActivity(intent);
        SharePreferenceUtils.saveIntSharedPreference(this,Constants.CONTANT_MODE, Constants.MODE.ALDULT);
        finish();
    }

    @Override
    public void addSuccess() {
        Toast.makeText(MainActivity.this,"Success",Toast.LENGTH_LONG).show();
    }

    private void initialData(){
        //Add question
        mAddQuestionPresenter = new AddQuestionPresenter(getApplication(), this);
        //Question for children
        Question question1= new Question(1,"Con vật nào sống dưới nước?","",1,0);
        Question question2= new Question(2,"Xe nào được quyền ưu tiên khi tham gia giao thông?","",1,0);
        Question question3= new Question(3,"Nhân vật nào xuất hiện trong truyện Thánh Gióng?" ,"",1,0);

        //Question for adult
        Question question4= new Question(4,"Một người uống nước dù không bỏ bất kì hạt muối nào vào ly nước đó nhưng vì sao người ta vẫn cảm thấy mặn?" ,"q1",2,0);
        Question question5= new Question(5,"Keo gì có thể đẩy mọi người xa nhau?" ,"q2",2,0);
        Question question6= new Question(6,"Quốc gia nào ở Đông Nam Á in lời quốc ca lên tờ tiền của nước mình?" ,"q3",2,0);
        Question question7= new Question(7,"Sông gì có thể phi nước đại được?" ,"q4",2,0);
        Question question8= new Question(8,"Khi nào giống đực có thể mang thai và sinh sản được?" ,"q5",2,0);
        Question question9= new Question(9,"Cái gì nằm giữa đầu và mình, bỏ dấu thêm ngã sẽ thành bữa ngon là chữ gì?" ,"q6",2,0);
        Question question10= new Question(10,"Không cần đến hôm qua hay hôm nay nhưng thiếu mai thì khó sống được là con gì?" ,"q7",2,0);
        Question question11= new Question(11,"Chuyện gì sẽ xảy ra khi làm rơi điện thoại di động xuống nước?" ,"q8",2,0);
        Question question12= new Question(12," Điều gì loài voi làm được còn những loài khác thì không?" ,"q9",2,0);
        Question question13= new Question(13," Cá lóc người miền trung gọi là cá gì?" ,"q10",2,0);
        Question question14= new Question(14," Thân em xưa ở bụi tre mùa đông xếp lại mùa hè mở ra là cái gì?" ,"q11",2,0);
        Question question15= new Question(15," Có ông làm nghề hái cà tím, vậy tiếp theo ông ta sẽ làm gì?" ,"q12",2,0);
        Question question16= new Question(16," Chuột cống, chuột nhắt, chuột chù, chuột đồng con nào khác những con còn lại?" ,"q13",2,0);
        Question question17= new Question(17," Chín cổ chín đầu đặt đâu nằm đó là con gì?" ,"q14",2,0);
        Question question18= new Question(18," Đường gì chỉ đi lên mà không đi xuống?" ,"q15",2,0);
        Question question19= new Question(19," Câu hỏi nào mà bạn ngày nào cũng hỏi và có được câu trả lời khác nhau nhưng vẫn chính xác?" ,"q16",2,0);
        Question question20= new Question(20," Ở chỗ nào mà hôm nay ở trước hôm qua?" ,"q17",2,0);
        Question question21= new Question(21," Một loại hạt quý báu nhưng có thể ăn được đó làm hạt gì?" ,"q18",2,0);
        Question question22= new Question(22," Làm thế nào để một tay che kín bầu trời?" ,"q19",2,0);
        Question question23= new Question(23," Con nào không biết ăn nhưng có tận 4 hàm răng?" ,"q20",2,0);
        List<Question> questionList= new ArrayList<>();
        questionList.add(question1);
        questionList.add(question2);
        questionList.add(question3);
        questionList.add(question4);
        questionList.add(question5);
        questionList.add(question6);
        questionList.add(question7);
        questionList.add(question8);
        questionList.add(question9);
        questionList.add(question10);
        questionList.add(question11);
        questionList.add(question12);
        questionList.add(question13);
        questionList.add(question14);
        questionList.add(question15);
        questionList.add(question16);
        questionList.add(question17);
        questionList.add(question18);
        questionList.add(question19);
        questionList.add(question20);
        questionList.add(question21);
        questionList.add(question22);
        questionList.add(question23);
        mAddQuestionPresenter.addListQuestion(questionList);
        //Add answer
        mAddAnswerPresenter= new AddAnswerPresenter(this, getApplication(), this);
        Answer answer1= new Answer("img_bo", false,1,1);
        Answer answer2= new Answer("img_chim", false,2,1);
        Answer answer3= new Answer("img_ca", true,3,1);
        Answer answer4= new Answer("img_khi", false,4,1);

        Answer answer5= new Answer("img_oto", false,1,2);
        Answer answer6= new Answer("img_xemay", false,2,2);
        Answer answer7= new Answer("img_xedap", false,3,2);
        Answer answer8= new Answer("img_cuuthuong", true,4,2);

        Answer answer9= new Answer("img_tonngokhong", false,1,3);
        Answer answer10= new Answer("img_thanhgiong", true,2,3);
        Answer answer11= new Answer("img_cobequangkhando", false,3,3);
        Answer answer12= new Answer("img_bachtuyet", false,4,3);

        Answer answer13= new Answer("UỐNG NƯỚC BIỂN", true,1,4);
        Answer answer14= new Answer("NGƯỜI ĐÓ UỐNG NƯỚC BIỂN", true,1,4);
        Answer answer15= new Answer("NƯỚC BIỂN", true,1,4);
        Answer answer16= new Answer("VÌ ĐÓ LÀ NƯỚC BIỂN", true,1,4);

        Answer answer17= new Answer("KEO KIỆT", true,1,5);

        Answer answer18= new Answer("SINGAPORE", true,1,6);

        Answer answer19= new Answer("SÔNG MÃ", true,1,7);

        Answer answer20= new Answer("CÁ NGỰA", true,1,8);
        Answer answer21= new Answer("CON CÁ NGỰA", true,1,8);

        Answer answer22= new Answer("CÁI CỔ", true,1,9);
        Answer answer23= new Answer("CẦN CỔ", true,1,9);
        Answer answer24= new Answer("CÁI CẦN CỔ", true,1,9);
        Answer answer37= new Answer("CỔ", true,1,9);

        Answer answer25= new Answer("CON RÙA", true,1,10);
        Answer answer26= new Answer("RÙA", true,1,10);

        Answer answer27= new Answer("ƯỚT", true,1,11);
        Answer answer28= new Answer("BỊ ƯỚT", true,1,11);

        Answer answer29= new Answer("ĐẺ VOI CON", true,1,12);
        Answer answer30= new Answer("SINH VOI CON", true,1,12);
        Answer answer31= new Answer("ĐẺ RA VOI CON", true,1,12);
        Answer answer32= new Answer("SINH RA VOI CON", true,1,12);

        Answer answer33= new Answer("CÁ CHUỐI", true,1,13);

        Answer answer34= new Answer("CÁI QUẠT", true,1,14);
        Answer answer35= new Answer("QUẠT", true,1,14);

        Answer answer36= new Answer("TÌM CÀ HÁI", true,1,15);

        Answer answer38= new Answer("CHUỘT ĐỒNG", true,1,16);
        Answer answer39= new Answer("CON CHUỘT ĐỒNG", true,1,16);

        Answer answer40= new Answer("CON GÀ LUỘC", true,1,17);
        Answer answer41= new Answer("CON GÀ LUỘC", true,1,17);
        Answer answer42= new Answer("CON GÀ CHÍN", true,1,17);
        Answer answer43= new Answer("CON GÀ BỊ LUỘC", true,1,17);
        Answer answer44= new Answer("CON GÀ ĐÃ CHÍN", true,1,17);
        Answer answer45= new Answer("CON GÀ CÚNG", true,1,17);
        Answer answer46= new Answer("GÀ CÚNG", true,1,17);
        Answer answer47= new Answer("GÀ CHÍN", true,1,17);
        Answer answer48= new Answer("GÀ LUỘC", true,1,17);

        Answer answer49= new Answer("ĐƯỜNG TĂNG", true,1,18);
        Answer answer50= new Answer("TĂNG", true,1,18);

        Answer answer51= new Answer("MẤY GIỜ", true,1,19);
        Answer answer52= new Answer("MẤY GIỜ RỒI", true,1,19);
        Answer answer53= new Answer("HỎI MẤY GIỜ", true,1,19);

        Answer answer54= new Answer("Ở TRONG TỪ ĐIỂN", true,1,20);
        Answer answer55= new Answer("TRONG TỪ ĐIỂN", true,1,20);
        Answer answer56= new Answer("TỪ ĐIỂN", true,1,20);

        Answer answer57= new Answer("HẠT TRÂN CHÂU", true,1,21);
        Answer answer58= new Answer("TRÂN CHÂU", true,1,21);

        Answer answer59= new Answer("CHE MẮT LẠI", true,1,22);
        Answer answer60= new Answer("BỊT MẮT LẠI", true,1,22);
        Answer answer61= new Answer("LẤY TAY CHE MẮT LẠI", true,1,22);
        Answer answer62= new Answer("LẤY TAY BỊT MẮT LẠI", true,1,22);

        Answer answer63= new Answer("CON TEM", true,1,23);
        Answer answer64= new Answer("TEM", true,1,23);
        Answer answer65= new Answer("TEM BƯU ĐIỆN", true,1,23);
        Answer answer66= new Answer("CON TEM BƯU ĐIỆN", true,1,23);
        List<Answer> answerList= new ArrayList<>();
        answerList.add(answer1);
        answerList.add(answer2);
        answerList.add(answer3);
        answerList.add(answer4);
        answerList.add(answer5);
        answerList.add(answer6);
        answerList.add(answer7);
        answerList.add(answer8);
        answerList.add(answer9);
        answerList.add(answer10);
        answerList.add(answer11);
        answerList.add(answer12);
        answerList.add(answer13);
        answerList.add(answer14);
        answerList.add(answer15);
        answerList.add(answer16);
        answerList.add(answer17);
        answerList.add(answer18);
        answerList.add(answer19);
        answerList.add(answer20);
        answerList.add(answer21);
        answerList.add(answer22);
        answerList.add(answer23);
        answerList.add(answer24);
        answerList.add(answer25);
        answerList.add(answer26);
        answerList.add(answer27);
        answerList.add(answer28);
        answerList.add(answer29);
        answerList.add(answer30);
        answerList.add(answer31);
        answerList.add(answer32);
        answerList.add(answer33);
        answerList.add(answer34);
        answerList.add(answer35);
        answerList.add(answer36);
        answerList.add(answer37);
        answerList.add(answer38);
        answerList.add(answer39);
        answerList.add(answer40);
        answerList.add(answer41);
        answerList.add(answer42);
        answerList.add(answer43);
        answerList.add(answer44);
        answerList.add(answer45);
        answerList.add(answer46);
        answerList.add(answer47);
        answerList.add(answer48);
        answerList.add(answer49);
        answerList.add(answer50);
        answerList.add(answer51);
        answerList.add(answer52);
        answerList.add(answer53);
        answerList.add(answer54);
        answerList.add(answer55);
        answerList.add(answer56);
        answerList.add(answer57);
        answerList.add(answer58);
        answerList.add(answer59);
        answerList.add(answer60);
        answerList.add(answer61);
        answerList.add(answer62);
        answerList.add(answer63);
        answerList.add(answer64);
        answerList.add(answer65);
        answerList.add(answer66);
        mAddAnswerPresenter.addListAnswer(answerList);
    }

    @Override
    public void showError() {
        Toast.makeText(MainActivity.this,"Failed",Toast.LENGTH_LONG).show();
    }

    @Override
    public void addAnswerSuccess() {
        SharePreferenceUtils.saveBooleanSharedPreference(this, Constants.IS_ADDED_QUETION, true);
    }

    private void checkAddedQuestion(){
        boolean isAdded = SharePreferenceUtils.getBooleanSharedPreference(this, Constants.IS_ADDED_QUETION);
        if(!isAdded){
            initialData();
        }
    }
}
