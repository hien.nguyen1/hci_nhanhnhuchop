package hiennt.nhanhnhuchop.activity;

import android.app.Application;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class NhanhNhuChopApplication extends Application{
    @Override
    public void onCreate() {
        super.onCreate();
        initialCalligraphy();
    }

    private void initialCalligraphy() {
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/FiraSans-Regular.ttf")
                .setFontAttrId(uk.co.chrisjenx.calligraphy.R.attr.fontPath)
                .build()
        );
    }
}
