package hiennt.nhanhnhuchop.activity;

import android.app.ActivityOptions;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import hiennt.nhanhnhuchop.R;
import hiennt.nhanhnhuchop.utils.Constants;
import hiennt.nhanhnhuchop.utils.SharePreferenceUtils;

public class SplashActivity extends BaseActivity {
    private ImageView mImgBg;
    private VideoView videoView;
    private MediaController mediaController;
    CountDownTimer mCountDownTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        initialView();
        countDownTimer();
    }

    private void initialView(){
        mImgBg= findViewById(R.id.img_bg);
        videoView= findViewById(R.id.videoView);
    }

    private void playVideo(){
        videoView.setVideoPath("android.resource://" + getPackageName() + "/" + R.raw.intro);
        mediaController = new MediaController(SplashActivity.this);
        mediaController.setAnchorView(videoView);
        videoView.setMediaController(mediaController);
        videoView.setMediaController(null);
        videoView.start();
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                intentToMainActivity();
            }
        });
    }

    private void countDownTimer() {
        mCountDownTimer = new CountDownTimer(4000, 2000) {
            @Override
            public void onTick(long l) {
            }

            @Override
            public void onFinish() {
//                mImgBg.setVisibility(View.GONE);
//                playVideo();
                checkMode();
            }
        }.start();
    }

    private void intentToMainActivity(){
        Intent intent= new Intent(SplashActivity.this, MainActivity.class);
        ActivityOptions options =
                ActivityOptions.makeCustomAnimation(SplashActivity.this, R.anim.fade_in, R.anim.fade_out);
        startActivity(intent, options.toBundle());
        finish();
    }

    private void intentToDashboardChild(){
        Intent intent= new Intent(SplashActivity.this, DashboardChildActivity.class);
        ActivityOptions options =
                ActivityOptions.makeCustomAnimation(SplashActivity.this, R.anim.fade_in, R.anim.fade_out);
        startActivity(intent, options.toBundle());
        finish();
    }

    private void intentToDashboardAdult(){
        Intent intent= new Intent(SplashActivity.this, DashboardAdultActivity.class);
        ActivityOptions options =
                ActivityOptions.makeCustomAnimation(SplashActivity.this, R.anim.fade_in, R.anim.fade_out);
        startActivity(intent, options.toBundle());
        finish();
    }

    private void checkMode(){
        int mode= SharePreferenceUtils.getIntSharedPreference(this, Constants.CONTANT_MODE);
        switch (mode){
            case Constants.MODE.NONE:
                intentToMainActivity();
                break;
            case Constants.MODE.CHILDREN:
                intentToDashboardChild();
                break;
            case Constants.MODE.ALDULT:
                intentToDashboardAdult();
                break;
        }
    }
}
