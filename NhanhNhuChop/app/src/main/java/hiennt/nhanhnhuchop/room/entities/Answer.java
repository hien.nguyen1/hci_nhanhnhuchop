package hiennt.nhanhnhuchop.room.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "Answer")
public class Answer implements Serializable {
    @PrimaryKey(autoGenerate = true)
    private int answerId;

    @ColumnInfo(name = "answer_content")
    private String answerContent;

    @ColumnInfo(name = "answer_check")
    private boolean answerCheck;

    @ColumnInfo(name = "position")
    private int position;

    @ColumnInfo(name = "question_id")
    private int questionId;

    public Answer() {
    }

    public Answer(String answerContent, boolean answerCheck, int position, int questionId) {
        this.answerContent = answerContent;
        this.answerCheck = answerCheck;
        this.position = position;
        this.questionId = questionId;
    }

    public int getAnswerId() {
        return answerId;
    }

    public void setAnswerId(int answerId) {
        this.answerId = answerId;
    }

    public String getAnswerContent() {
        return answerContent;
    }

    public void setAnswerContent(String answerContent) {
        this.answerContent = answerContent;
    }

    public boolean isAnswerCheck() {
        return answerCheck;
    }

    public void setAnswerCheck(boolean answerCheck) {
        this.answerCheck = answerCheck;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }
}
