package hiennt.nhanhnhuchop.room.daos;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import hiennt.nhanhnhuchop.room.entities.Question;

@Dao
public interface QuestionDao {
    @Insert
    void insertQuestion(Question... questions);

    @Delete
    void deleteQuestion(Question... questions);

    //delete all user
    @Query("DELETE FROM question")
    void deleleAllQuestion();

    //get question by type
    @Query("select * from question where question_type =:type")
    List<Question> getQuestionByType(int type);
}
