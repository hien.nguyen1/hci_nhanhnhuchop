package hiennt.nhanhnhuchop.room.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "Question")
public class Question implements Serializable{
    @PrimaryKey(autoGenerate = true)
    private int questionIdAI;

    @ColumnInfo(name = "question_id")
    private int questionId;

    @ColumnInfo(name = "question_content")
    private String questionContent;

    @ColumnInfo(name = "voice")
    private String voice;

    @ColumnInfo(name = "question_type")
    private int questionType;

    @ColumnInfo(name = "mc")
    private int mc;

    public Question() {
    }

    public Question(int questionId, String questionContent, String voice, int questionType, int mc) {
        this.questionId = questionId;
        this.questionContent = questionContent;
        this.voice = voice;
        this.questionType = questionType;
        this.mc = mc;
    }

    public int getQuestionIdAI() {
        return questionIdAI;
    }

    public void setQuestionIdAI(int questionIdAI) {
        this.questionIdAI = questionIdAI;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public String getQuestionContent() {
        return questionContent;
    }

    public void setQuestionContent(String questionContent) {
        this.questionContent = questionContent;
    }

    public String getVoice() {
        return voice;
    }

    public void setVoice(String voice) {
        this.voice = voice;
    }

    public int getQuestionType() {
        return questionType;
    }

    public void setQuestionType(int questionType) {
        this.questionType = questionType;
    }

    public int getMc() {
        return mc;
    }

    public void setMc(int mc) {
        this.mc = mc;
    }
}
