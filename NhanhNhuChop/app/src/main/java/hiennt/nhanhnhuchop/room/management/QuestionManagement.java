package hiennt.nhanhnhuchop.room.management;

import android.app.Application;
import android.database.sqlite.SQLiteConstraintException;
import android.os.AsyncTask;

import java.util.List;

import hiennt.nhanhnhuchop.room.daos.QuestionDao;
import hiennt.nhanhnhuchop.room.database.NNCDatabase;
import hiennt.nhanhnhuchop.room.entities.Answer;
import hiennt.nhanhnhuchop.room.entities.Question;

public class QuestionManagement {
    private QuestionDao mQuestionDao;
    private Application mApplication;

    public QuestionManagement(Application application) {
        this.mApplication = application;
        NNCDatabase nncDatabase = NNCDatabase.getDatabase(mApplication);
        mQuestionDao = nncDatabase.questionDao();
    }

    public void addQuestion(Question question, OnDataCallBackQuestion listener) {
        AddQuestionAsync addQuestionAsync = new AddQuestionAsync(mQuestionDao, listener);
        addQuestionAsync.execute(question);
    }

    public void getQuestionByType(int type, OnDataCallBackListQuestion listener) {
        GetQuestionAsync getQuestionAsync = new GetQuestionAsync(mQuestionDao, listener,type );
        getQuestionAsync.execute();
    }

    public interface OnDataCallBackQuestion {
        void onDataSuccess(Question question);

        void onDataFail();
    }

    public interface OnDataCallBackListQuestion {
            void onDataSuccess(List<Question> questionList);

            void onDataFail();
        }

    private class AddQuestionAsync extends AsyncTask<Question, Void, Void> {
        private QuestionDao mDaoAsync;
        private OnDataCallBackQuestion mListener;

        public AddQuestionAsync(QuestionDao mDaoAsync, OnDataCallBackQuestion listener) {
            this.mDaoAsync = mDaoAsync;
            this.mListener = listener;
        }

        @Override
        protected Void doInBackground(Question... questions) {
            try {
                mDaoAsync.insertQuestion(questions);
            } catch (SQLiteConstraintException e) {

            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mListener.onDataSuccess(null);
        }
    }

    private class GetQuestionAsync extends AsyncTask<Question, Void, Void> {
        private QuestionDao mDaoAsync;
        private List<Question> mQuestionList;
        private OnDataCallBackListQuestion mListener;
        private int type;

        public GetQuestionAsync(QuestionDao mUserDaoAsync, OnDataCallBackListQuestion mListener, int type) {
            this.mDaoAsync = mUserDaoAsync;
            this.mListener = mListener;
            this.type= type;
        }


        @Override
        protected Void doInBackground(Question... customers) {
            try {
                mQuestionList = mDaoAsync.getQuestionByType(type);
            } catch (SQLiteConstraintException e) {
                mListener.onDataFail();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (mQuestionList != null) {
                mListener.onDataSuccess(mQuestionList);
            } else {
                mListener.onDataFail();
            }
        }
    }
}