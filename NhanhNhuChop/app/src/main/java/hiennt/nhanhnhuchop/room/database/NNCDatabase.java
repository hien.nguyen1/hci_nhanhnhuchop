package hiennt.nhanhnhuchop.room.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import hiennt.nhanhnhuchop.room.daos.AnswerDao;
import hiennt.nhanhnhuchop.room.daos.QuestionDao;
import hiennt.nhanhnhuchop.room.entities.Answer;
import hiennt.nhanhnhuchop.room.entities.Question;

import static hiennt.nhanhnhuchop.room.database.NNCDatabase.DATABASE_VERSION;

@Database(entities = {Question.class,Answer.class}, exportSchema = false, version = DATABASE_VERSION)
public abstract class NNCDatabase extends RoomDatabase{
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "nnc-database";
    private static NNCDatabase INSTANCE;

    public abstract QuestionDao questionDao();
    public  abstract AnswerDao answerDao();
    public static NNCDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (NNCDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(), NNCDatabase.class, DATABASE_NAME)
                            .build();
                }
            }

        }
        return INSTANCE;
    }
}
