package hiennt.nhanhnhuchop.room.management;

import android.app.Application;
import android.database.sqlite.SQLiteConstraintException;
import android.os.AsyncTask;

import java.util.List;

import hiennt.nhanhnhuchop.room.daos.AnswerDao;
import hiennt.nhanhnhuchop.room.database.NNCDatabase;
import hiennt.nhanhnhuchop.room.entities.Answer;
import hiennt.nhanhnhuchop.room.entities.Question;

public class AnswerManagement {
    private AnswerDao mAnswerDao;
    private Application mApplication;

    public AnswerManagement(Application application) {
        this.mApplication = application;
        NNCDatabase nncDatabase = NNCDatabase.getDatabase(mApplication);
        mAnswerDao = nncDatabase.answerDao();
    }

    public void addAnswer(Answer answer, OnDataCallBackAnswer listener) {
        AddAnswerAsync addAnswerAsync = new AddAnswerAsync(mAnswerDao, listener);
        addAnswerAsync.execute(answer);
    }

    public void getAnswerByQuestionId(int questionId, OnDataCallBackListAnswer listener) {
        GetAnswerAsync getAnswerAsync = new GetAnswerAsync(mAnswerDao, listener,questionId );
        getAnswerAsync.execute();
    }

    public interface OnDataCallBackAnswer {
        void onDataSuccess(Question question);

        void onDataFail();
    }

    public interface OnDataCallBackListAnswer {
        void onDataSuccess(List<Answer> answerList);

        void onDataFail();
    }

    private class AddAnswerAsync extends AsyncTask<Answer, Void, Void> {
        private AnswerDao mDaoAsync;
        private OnDataCallBackAnswer mListener;

        public AddAnswerAsync(AnswerDao mDaoAsync, OnDataCallBackAnswer listener) {
            this.mDaoAsync = mDaoAsync;
            this.mListener = listener;
        }

        @Override
        protected Void doInBackground(Answer... answers) {
            try {
                mDaoAsync.insertAnswer(answers);
            } catch (SQLiteConstraintException e) {

            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mListener.onDataSuccess(null);
        }
    }

    private class GetAnswerAsync extends AsyncTask<Question, Void, Void> {
        private AnswerDao mDaoAsync;
        private List<Answer> mAnswerList;
        private OnDataCallBackListAnswer mListener;
        private int type;

        public GetAnswerAsync(AnswerDao mUserDaoAsync, OnDataCallBackListAnswer mListener, int type) {
            this.mDaoAsync = mUserDaoAsync;
            this.mListener = mListener;
            this.type= type;
        }


        @Override
        protected Void doInBackground(Question... customers) {
            try {
                mAnswerList = mDaoAsync.getAnswerByQuestionId(type);
            } catch (SQLiteConstraintException e) {
                mListener.onDataFail();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (mAnswerList != null) {
                mListener.onDataSuccess(mAnswerList);
            } else {
                mListener.onDataFail();
            }
        }
    }
}
