package hiennt.nhanhnhuchop.room.daos;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import hiennt.nhanhnhuchop.room.entities.Answer;
import hiennt.nhanhnhuchop.room.entities.Question;

@Dao
public interface AnswerDao {
    @Insert
    void insertAnswer(Answer... answers);

    @Delete
    void deleteAnswer(Answer... answers);

    //delete all user
    @Query("DELETE FROM answer")
    void deleleAllAnswer();

    //get question by type
    @Query("select * from answer where question_id =:questionId")
    List<Answer> getAnswerByQuestionId(int questionId);
}
