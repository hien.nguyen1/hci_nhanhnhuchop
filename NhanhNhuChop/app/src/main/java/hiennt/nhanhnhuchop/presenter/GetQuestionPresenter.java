package hiennt.nhanhnhuchop.presenter;

import android.app.Application;
import android.content.Context;

import java.util.List;

import hiennt.nhanhnhuchop.room.entities.Question;
import hiennt.nhanhnhuchop.room.management.QuestionManagement;
import hiennt.nhanhnhuchop.view.GetQuestionView;

public class GetQuestionPresenter {
    private Context mContext;
    private QuestionManagement mQuestionManagement;
    private GetQuestionView mGetQuestionView;

    public GetQuestionPresenter(Context context, Application application, GetQuestionView getQuestionView){
        this.mContext= context;
        this.mQuestionManagement= new QuestionManagement(application);
        this.mGetQuestionView= getQuestionView;
    }

    public void getQuestion(int type){
        mQuestionManagement.getQuestionByType(type, new QuestionManagement.OnDataCallBackListQuestion() {
            @Override
            public void onDataSuccess(List<Question> questionList) {
                if(questionList!= null) {
                    mGetQuestionView.showQuestion(questionList);
                }else {
                    mGetQuestionView.showError();
                }
            }

            @Override
            public void onDataFail() {
                mGetQuestionView.showError();
            }
        });
    }
}
