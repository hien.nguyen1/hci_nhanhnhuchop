package hiennt.nhanhnhuchop.presenter;

import android.app.Application;
import android.content.Context;

import java.util.List;

import hiennt.nhanhnhuchop.room.entities.Answer;
import hiennt.nhanhnhuchop.room.entities.Question;
import hiennt.nhanhnhuchop.room.management.AnswerManagement;
import hiennt.nhanhnhuchop.view.AddAnswerView;

public class AddAnswerPresenter {
    private Context mContext;
    private AnswerManagement mAnswerManagement;
    private AddAnswerView mAddAnswerView;
    private int total;
    private int success=0;

    public AddAnswerPresenter(Context context, Application application, AddAnswerView addAnswerView){
        this.mContext= context;
        this.mAnswerManagement= new AnswerManagement(application);
        this.mAddAnswerView= addAnswerView;
    }

    public void addAnswer(Answer answer){
        mAnswerManagement.addAnswer(answer, new AnswerManagement.OnDataCallBackAnswer() {
            @Override
            public void onDataSuccess(Question question) {
                success++;
                mAddAnswerView.addAnswerSuccess();
            }

            @Override
            public void onDataFail() {
                total--;
                if(total==0){
                    mAddAnswerView.showError();
                }
            }
        });
    }

    public void addListAnswer(List<Answer> answerList){
        total= answerList.size();
        for (int i=0; i<total;i++){
            if(success==total){
                mAddAnswerView.addAnswerSuccess();
            }else {
                addAnswer(answerList.get(i));
            }
        }
    }
}
