package hiennt.nhanhnhuchop.presenter;

import android.app.Application;
import android.content.Context;

import java.util.List;

import hiennt.nhanhnhuchop.room.entities.Answer;
import hiennt.nhanhnhuchop.room.management.AnswerManagement;
import hiennt.nhanhnhuchop.view.GetAnswerView;

public class GetAnswerPresenter {
    private Context mContext;
    private AnswerManagement mAnswerManagement;
    private GetAnswerView mGetAnswerView;

    public GetAnswerPresenter(Context context, Application application, GetAnswerView getAnswerView){
        this.mContext= context;
        this.mAnswerManagement= new AnswerManagement(application);
        this.mGetAnswerView= getAnswerView;
    }

    public void getAnswer(int questionId){
        mAnswerManagement.getAnswerByQuestionId(questionId, new AnswerManagement.OnDataCallBackListAnswer() {
            @Override
            public void onDataSuccess(List<Answer> answerList) {
                if(answerList!= null){
                    mGetAnswerView.showAnswerSuccess(answerList);
                }else {
                    mGetAnswerView.showError();
                }
            }

            @Override
            public void onDataFail() {
                mGetAnswerView.showError();
            }
        });
    }
}
