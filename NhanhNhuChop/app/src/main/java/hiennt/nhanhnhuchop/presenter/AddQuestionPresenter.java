package hiennt.nhanhnhuchop.presenter;

import android.app.Application;

import java.util.List;

import hiennt.nhanhnhuchop.room.entities.Question;
import hiennt.nhanhnhuchop.room.management.QuestionManagement;
import hiennt.nhanhnhuchop.view.AddQuestionView;

public class AddQuestionPresenter {
    private QuestionManagement mQuestionManagement;
    private AddQuestionView mAddQuestionView;
    private int total;
    private int success=0;

    public AddQuestionPresenter(Application application, AddQuestionView addQuestionView) {
        this.mQuestionManagement = new QuestionManagement(application);
        this.mAddQuestionView = addQuestionView;
    }

    public void addQuestion(Question question) {
        mQuestionManagement.addQuestion(question, new QuestionManagement.OnDataCallBackQuestion() {
            @Override
            public void onDataSuccess(Question question) {
                success++;
            }

            @Override
            public void onDataFail() {
                total--;
                if(total==0) {
                    mAddQuestionView.showError();
                }
            }
        });
    }

    public void addListQuestion(List<Question> questionList){
        total= questionList.size();
        for (int i=0; i<total;i++){
            if(success==total){
                mAddQuestionView.addSuccess();
            }else {
                addQuestion(questionList.get(i));
            }
        }
    }
}
