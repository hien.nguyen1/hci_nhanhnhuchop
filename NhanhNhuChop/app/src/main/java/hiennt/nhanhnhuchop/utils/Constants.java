package hiennt.nhanhnhuchop.utils;

public class Constants {
    public static final String IS_ADDED_QUETION="isAddedQuestion";
    public static final int REQ_CODE_SPEECH_INPUT = 100;

    public interface MODE{
        int NONE = 0;
        int CHILDREN = 1;
        int ALDULT = 2;
    }

    public static final String CONTANT_MODE= "mode";
}
