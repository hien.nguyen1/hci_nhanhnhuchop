package hiennt.nhanhnhuchop.view;

import java.util.List;

import hiennt.nhanhnhuchop.room.entities.Question;

public interface GetQuestionView extends BaseView{
    void showQuestion(List<Question> question);

}
