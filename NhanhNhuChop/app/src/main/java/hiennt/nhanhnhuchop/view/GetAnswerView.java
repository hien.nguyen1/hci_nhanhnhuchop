package hiennt.nhanhnhuchop.view;

import java.util.List;

import hiennt.nhanhnhuchop.room.entities.Answer;

public interface GetAnswerView extends BaseView{
    void showAnswerSuccess(List<Answer> answerList);
}
